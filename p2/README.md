| Developed by: Conner A. Bradley |
| ------------------------------: |
# Web Application Development Project 2

***
***

### Requirements:

1. Code data entry forms with basic client/server-side validation using regular expressions and prepared statements to help prevent SQL injection. 
1. Implement JSTL to prevent XSS into your web application and complete CRUD functionality.
1. Develop README.md in markdownload language and push to repository when finished. 
1. Display screencaptures of your results.
1. Review the chapter questions. 


### Results:
>**NOTE:** Click any screencapture to view an enlarged version

| *Valid Data*: | *Passed Data*: | *Display Customer Table*: |
|:---------------:|:---------------:| :---------------:|
| [![Valid Data Screenshot](img/valid_data.png "Data valid")](img/valid_data.png) | [![Data Passed Screenshot](img/passed_data.png "Data passed")](img/passed_data.png) | [![Data Display](img/display_data.png "Table")](img/display_data.png) |

| *Update Data*: | *Modified Data*: | *Delete Check*: |
|:-------------------------------------------:|:-------------------------------------------:|:------------------------------------------------:|
[![update_data Screenshot](img/update_data.png)](img/update_data.png) | [![delete_check Screenshot](img/delete_check.png)](img/delete_check.png) | [![modified_data Screenshot](img/modified_data.png)](img/modified_data.png)

| *SQL*: |
|:------------------------------------------------:|
| [![SQL Screenshot](img/sql.png)](img/sql.png) |

### Referenced in README.md:

* project requirements
* valid_data.png
* passed_data.png
* display_data.png
* sql.png
* update_data.png
* delete_check.png
* modified_data.png