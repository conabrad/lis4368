
| Developed by: Conner A. Bradley |
| ------------------------------: |
# Web Application Development Assignment 3 

***
***

### Requirements:

1. Design an Entity Relationship Diagram (ERD) for a mock-petstore.
1. Forward engineer the ERD with data (minimum of 10 records in each table).
1. Develop README.md in markdownload language and push to repository when finished. 
1. Display screencaptures of your results.
1. Include links to a3.sql and a3.mwb.
1.  Develop separate programs to do the following:
    * SS4 - Lists files and subdirectories of a user-specified directory.
    * SS5 - Determines total number of characters in a user-entered line of text, as well as the number of times a specific character is used and its ASCII value. 
    * SS6 - Determines whether user-entered value is a vowel, consonant, special character, or an integer. Display's character's ASCII value. 
1. Review the chapter questions for chapters 7 - 8. 



### Results:
>**NOTE:** Click any screencapture to view an enlarged version


| *ERD*: | *Customer Table* | *Pet Table* | *Petstore Table* |
|:-----------------------:|:------------------------------:|:-------------------------:|:------------------------------:|
| [![ERD Screenshot](img/a3_ERD.png "ERD based up A3 requirements")](img/a3_ERD.png) | [![Customer Table Screenshot](img/cus_table.png "ERD Customer Table")](img/cus_table.png) | [![Pet Table Screenshot](img/pet_table.png "ERD Pet Table")](img/pet_table.png) | [![Petstore Screenshot](img/pts_table.png "ERD Petstore Table")](img/pts_table.png) |

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")
[A3 SQL File](docs/a3.sql "A3 SQL Script")

| *Directory Info*: | *Character Info*: | *Determine Character*: |
|:-------------------------------------------:|:-------------------------------------------:|:------------------------------------------------:|
[![Q4_directory_info Screenshot](img/Q4_directory_info.png)](img/Q4_directory_info.png) | [![Q5_character_info Screenshot](img/Q5_character_info.png)](img/Q5_character_info.png) | [![Q6_determine_character Screenshot](img/Q6_determine_character.png)](img/Q6_determine_character.png)


### Referenced in README.md:

* links to the following:
    * a3.sql, a3.mwb
* a3.png
* skillsets:
    * Q4_directory_info.png
    * Q5_character_info.png
    * Q6_determine_character.png
* README.md (must display all pngs)