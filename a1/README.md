
| Developed by: Conner A. Bradley |
| ------------------------------: |
# Web Application Development Assignment 1 

***
***

### Requirements:

1. Utilize a Distributed Version Control System with Git and BitBucket to establish a repository for your assignments and projects. 
1. Install Apache Tomcat, AMPPS, and Java/JSP/Servlet development environnment (JDK) to your machine.
1. Develop README.md in markdownload language and push to repository when finished. 
1. Display screencaptures of your results.
1. Include Git commands with descriptions in README.md.
1. Review the chapter questions for chapters 1 - 4. 


### Git Commands:

1. "git init" - one way to start a new project with Git
2. "git status" - shows status of working tree
3. "git add" - add files to staging area
4. "git commit -m 'message'" - creates and saves snapshot of changes
5. "git push" - send local commits to the master branch of the remote repo
6. "git pull" - merges all changes in the remote repo with the local pwd
7. "git remote -v" - views all remote repos

### Results:
>**NOTE:** Click any screencapture to view an enlarged version

| *Running Java Hello*: | *Running http://localhost:9999*: | *Running http://localhost:9999/LIS4368/a1*: |
|:---------------------:|:--------------------------------:|:-------------------------------------------:|
 [![Java Hello Running Screenshot](img/jdk_install.png)](img/jdk_install.png) | [![http://localhost:9999 Running Screenshot](img/tomcat.png)](img/tomcat.png) | [![http://localhost:9999/LIS4368/a1 Running Screenshot](img/a1.png)](img/a1.png) 

### Referenced in README.md: 
* assignment requirements
* git commands and a short description
* jdk_install.png
* tomcat.png
* a1.png 


### Tutorial Links :
>**NOTE:** This link is undergoing maintentance

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/conabrad/bitbucketstationlocations/ "Bitbucket Station Locations")

