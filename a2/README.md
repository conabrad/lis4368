
| Developed by: Conner A. Bradley |
| ------------------------------: |
# Web Application Development Assignment 2 

***
***

### Requirements:

1. Startup AMPPS and Tomcat.
1. Setup JavaServlets.
1. Connect a database to your web application.
1. Develop separate programs to do the following:
    * SS1 - Lists system properties.
    * SS2 - Loops through an array of floats using the for, enhanced for, while, and do...while looping structures. 
    * SS3 - Swaps two user-entered floating point values using two user-defined methods. Performs data-validation and prints numbers before and after swap. 
1. Develop README.md in markdownload language and push to repository when finished. 
1. Display screencaptures of your results.
1. Include URLs to assess you web application's functionality in README.md. 
1. Review the chapter questions for chapters 5 - 6. 


### Assessment links:

1. http://localhost:9999/hello
2. http://localhost:9999/hello/HelloHome.html
3. http://localhost:9999/hello/index.html
4. http://localhost:9999/hello/sayhello
5. http://localhost:9999/hello/querybook.html


### Results:
>**NOTE:** Click any screencapture to view an enlarged version

| *Servlet Compilation and Usage:* | *Database Connectivity Request Using Servlets:* |
|:----------------------------------------------:|:-------------------------------------------------------------:|
[![Screenshot of Servlet Compilation and Usage](img/say_hello.png)](img/say_hello.png) | [![Screenshot of Database Connectivity Request Using Servlets](img/querybook.png)](img/querybook.png)

|*Database Connectivity Result Using Servlets:* | *a2/index.jsp:* |
|:-----------------------------------------------------------:|:-----------------------------:|
 [![Screenshot of Database Connectivity Result Using Servlets](img/query_results.png)](img/query_results.png) | [![Screenshot of a2/index.jsp](img/a2_index.png)](img/a2_index.png)

| *System's Properties*: | *Looping Structures*: | *Number Swap*: |
|:----------------------:|:---------------------:|:--------------:|
| [![Screenshot of Skillset Q1](img/Q1_system_properties.png)](img/Q1_system_properties.png) | [![Screenshot of Skillset Q2](img/Q2_looping_structures.png)](img/Q2_looping_structures.png) | [![Screenshot of Skillset Q3](img/Q3_number_swap.png)](img/Q3_number_swap.png) |


### Referenced in README.md: 

* assignment requirements
* web application assessment URLs
* say_hello.png
* querbook.png
* query_results.png
* a2_index.png
* skillsets:
    * Q1_system_properties.png
    * Q2_looping_structures.png
    * Q3_number_swap.png

