
| Developed by: Conner A. Bradley |
| ------------------------------: |
# Web Application Development

***
***

### FSU School of Commnication and Information LIS4368 Requirements

Course Work Links

- [Assignment 1](a1/README.md "My a1 README.md file")
    1. Utilize a Distributed Version Control System with Git and BitBucket to establish a repository for your assignments and projects. 
    1. Install Apache Tomcat, AMPPS, and Java/JSP/Servlet development environnment (JDK) to your machine.
    1. Develop README.md in markdownload language and push to repository when finished. 
    1. Display screencaptures of your results.
    1. Include Git commands with descriptions in README.md.
    1. Review the chapter questions for chapters 1 - 4. 

- [Assignment 2](a2/README.md "My a2 README.md file")
    1. Startup AMPPS and Tomcat.
    1. Setup JavaServlets.
    1. Connect a database to your web application.
    1. Develop separate programs to do the following:
        * SS1 - Lists system properties.
        * SS2 - Loops through an array of floats using the for, enhanced for, while, and do...while looping structures. 
        * SS3 - Swaps two user-entered floating point values using two user-defined methods. Performs data-validation and prints numbers before and after swap. 
    1. Develop README.md in markdownload language and push to repository when finished. 
    1. Display screencaptures of your results.
    1. Include URLs to assess you web application's functionality in README.md. 
    1. Review the chapter questions for chapters 5 - 6. 

- [Assignment 3](a3/README.md "My a3 README.md file")
    1. Design an Entity Relationship Diagram (ERD) for a mock-petstore.
    1. Forward engineer the ERD with data (minimum of 10 records in each table).
    1. Develop README.md in markdownload language and push to repository when finished. 
    1. Display screencaptures of your results.
    1. Include links to a3.sql and a3.mwb.
    1.  Develop separate programs to do the following:
        * SS4 - Lists files and subdirectories of a user-specified directory.
        * SS5 - Determines total number of characters in a user-entered line of text, as well as the number of times a specific character is used and its ASCII value. 
        * SS6 - Determines whether user-entered value is a vowel, consonant, special character, or an integer. Display's character's ASCII value. 
    1. Review the chapter questions for chapters 7 - 8. 

- [Assignment 4](a4/README.md "My a4 README.md file")
    1. Code data entry forms with basic server-side validation using regular expressions and pass/fail tests into your web application. 
    1. Develop separate programs to do the following:
        * SS10 - Perform addition, subtraction, multiplication, division, and powers floating point numbers. Rounds to two decimal places. Checks for non-numeric values and division by zero.
        * SS11 - Calculate amount of money saved for a period of years, at a monthly compound interest rate. Returned amount is formatted in U.S. currency, and rounded to two decimal places. Performs data validation for non-numeric values, as well as only integer values for years. 
        * SS12 - Create a string array (str1) based upon users' number of preferred programming languages. Create a second string array (str2) based upon the length of str1 array. Copy str1 array elements into str2. Print elements of both arrays using Java's enhanced for loop. 
    1. Develop README.md in markdownload language and push to repository when finished. 
    1. Display screencaptures of your results.
    1. Review the chapter questions for chapters 11 - 12. 
- [Assignment 5](a5/README.md "My a5 README.md file")
    1. Code data entry forms with basic server-side validation using regular expressions and pass/fail tests into your web application. 
    1. Develop separate programs to do the following:
        * SS13 - Read and write into a file.
        * SS14 - Use Object Oriented Programming to get and set vehicle year, make, and model in constructor objects. 
        * SS15 - Use inheritance to get and set vehicle speed into the previous skillset's vehcile construct. 
    1. Develop README.md in markdownload language and push to repository when finished. 
    1. Display screencaptures of your results.
    1. Review the chapter questions for chapters 13 - 15.  
- [Project 1](p1/README.md "My p1 README.md file")
    1. Design your web application's splash page.
    1. Code data entry forms with basic client-side validation using regular expressions and pass/fail tests into your web application. 
    1. Develop separate programs to do the following:
        * SS7 - Count numbers and types of characters from a user-entered string.
        * SS8 - Lists all ASCII values for characters and outputs a character based on a user-entered ASCII value.
        * SS9 - Returns a letter grade based upon a user-entered numeric score.
    1. Develop README.md in markdownload language and push to repository when finished. 
    1. Display screencaptures of your results.
    1. Review the chapter questions for chapters 9 - 10. 

- [Project 2](p2/README.md "My p2 README.md file")
    1. Code data entry forms with basic client/server-side validation using regular expressions and prepared statements to help prevent SQL injection. 
    1. Implement JSTL to prevent XSS into your web application and complete CRUD functionality.
    1. Develop README.md in markdownload language and push to repository when finished. 
    1. Display screencaptures of your results.
    1. Review the chapter questions. 