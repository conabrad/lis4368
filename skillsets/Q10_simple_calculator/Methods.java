import java.util.Scanner;

public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("1. Perform addition, subtraction, multiplication, division, and powers floating point numbers. Rounds to two decimal places." 
        + "\n2. Checks for non-numeric values and division by zero.");
    }
    
    public static void calculateNumbers()
    {
        //initiazing variables and create Scanner obj
        double num1 = 0.0, num2 = 0.0;
        char operation = ' '; //char needs character here
        //String operation = "";
        Scanner sc = new Scanner(System.in);

        System.out.print("(a=addition, s=subtraction, m=mulitplication, d=division, e=exponent)" 
        + "\nEnter mathematical operation's associated letter:");
        //next() function return next token
        //generally, identifiers, keywords, literals operators and puncts
        //white space is not token

        //chain intrinsic (aka built-in) methods:
        //captures first character from user-entered token, and converts to lower case
        operation = sc.next().toLowerCase().charAt(0);

        while (operation !='a' && operation !='s' && operation !='m' && operation !='d' && operation !='e')
        {
            System.out.print("\nIncorrect operation. Please incorrect a valid option: ");
            operation = sc.next().toLowerCase().charAt(0);
            //operation = sc.next();
        }

        System.out.print("Please enter first number: ");
        while(!sc.hasNextDouble())
        {
            System.out.print("Not valid number!\n");
            sc.next();
            System.out.print("Please try again. Enter second number: ");
        }
        num1 = sc.nextDouble();

        System.out.print("Please enter second number: ");
        while(!sc.hasNextDouble())
        {
            System.out.print("Not valid number!\n");
            sc.next();
            System.out.print("Please try again. Enter second number: ");
        }
        num2 = sc.nextDouble();

        //test operation
        if(operation == 'a')
        {
            Addition(num1, num2);
        }
        
        else if(operation == 's')
        {
            Subtraction(num1, num2);
        }

        else if(operation == 'm')
        {
            Multiplication(num1, num2);
        }
        
        else if(operation == 'd')
        {
            if (num2 == 0) 
            {
                System.out.println("Cannot divide by zero.");
            }
            else
            {
                Division(num1, num2);
            }
        }
        
        else if(operation == 'e')
        {
            Exponential(num1, num2);
        }
        
        System.out.print("");
        sc.close();
    }

    public static void Addition(double n1, double n2)
    {
        System.out.print(n1 + " + " + n2 + " = ");
        System.out.printf("%.2f",(n1+n2));
    }

    public static void Subtraction(double n1, double n2)
    {
        
        System.out.print(n1 + " - " + n2 + " = ");
        System.out.printf("%.2f",(n1-n2));
    }

    public static void Multiplication(double n1, double n2)
    {
        
        System.out.print(n1 + " * " + n2 + " = ");
        System.out.printf("%.2f",(n1*n2));
    }

    public static void Division(double n1, double n2)
    {
        
        System.out.print(n1 + " / " + n2 + " = ");
        System.out.printf("%.2f",(n1/n2));
    }

    public static void Exponential(double n1, double n2)
    {
        
        System.out.print(n1 + " to the power of " + n2 + " = ");
        System.out.printf("%.2f",(Math.pow(n1,n2)));
    }


}
