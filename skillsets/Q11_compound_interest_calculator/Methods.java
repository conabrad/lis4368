import java.util.Scanner;
import java.text.NumberFormat;
import java.util.Locale;

public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("1. Calculate amount of money saved for a period of years, at a monthly compound interest rate." 
        + "\n2. Returned amount is formatted in U.S. currency, and rounded to two decimal places." 
        + "\n3. Performs data validation for non-numeric values, as well as only integer values for years.");
        System.out.println();
    }
    
    public static void getInterestValues()
    {
        Scanner sc = new Scanner(System.in);
        double principal = 0.0; //initial amount
        double rate = 0.0; // percent rate
        int time = 0; //years

        //prompt user 
        //hasNextDouble(); returns true if next token in scanner input can be interpreted as dobuble
        System.out.print("Current principal: $");
        while(!sc.hasNextDouble())
        {
            System.out.println("Not valid number!\n");
            sc.next();
            System.out.print("Please try again. Enter principal: $");
        }
        principal =  sc.nextDouble();

        System.out.print("Interest Rate (per year): ");
        while(!sc.hasNextDouble())
        {
            System.out.println("Not valid number!\n");
            sc.next();
            System.out.print("Please try again. Enter interest rate: $");
        }
        rate =  sc.nextDouble();

        System.out.print("Total Time (per year): ");
        while(!sc.hasNextInt())
        {
            System.out.println("Not valid number!\n");
            sc.next();
            System.out.print("Please try again. Enter years: $");
        }
        time =  sc.nextInt();

        sc.close();

        calculateInterest(principal, rate, time);
    }

    public static void calculateInterest(double p, double r, int t)
    {
        int n = 12; //number of times per year interest compounded (monthly)
        double i = 0.0; //interest
        double a = 0.0; //amount

        a = p * Math.pow(1 + (r/n), n*t);
        i = a - p;

        r = r * 100; //convert back to perc val

        //us currency
        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
        System.out.println("\nInitial Principal: " + nf.format(p));
        
        System.out.printf("Annual interest rate: %.1f%%%n",r);
        System.out.println("Total monthly compound interest after " + " years: " + nf.format(i));
        System.out.println("Total amount: " + nf.format(a));
    }
}
