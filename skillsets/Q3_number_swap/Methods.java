import java.util.Scanner;

public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("1. Program swaps two user entered floating point values.");
        System.out.println("2. Create at least two user-defined methods: getRequirements() and swapNumbers().");
        System.out.println("3. Must perform data validation.");
        System.out.println("4. Print numbers before/after swapping.");
    }
    
    //non-value returning method accepts int array arg (static requires no 0bject)
    public static void swapNums()
    {
        Scanner sc = new Scanner(System.in);
        float num1 = 0.0f;
        float num2 = 0.0f;
        float temp = 0.0f;

        //prompt user 
        //hasNextFloat(): returns true if next token in scanner's input can be interpreted as float value
        System.out.print("Enter your first value: ");
        while(!sc.hasNextFloat())
        {
            System.out.println("Invalid input!\n");
            sc.next();//important! if omitted, will go into infinite loop on invalid input!
            System.out.print("Please try again. Enter your first value: ");
        }
        num1 = sc.nextFloat();//valid input

        //prompt user for second value
        //hasNextFloat(): returns true if next token in scanner's input can be interpreted as float value
        System.out.print("\nEnter your second value: ");
        while(!sc.hasNextFloat())
        {
            System.out.println("Invalid input!\n");
            sc.next();//important! if omitted, will go into infinite loop on invalid input!
            System.out.print("Please try again. Enter your second value: ");
        }
        num2 = sc.nextFloat();//valid input

        System.out.println("\nBefore swap:");
        System.out.println("Value 1 = " + num1);
        System.out.println("Value 2 = " + num2);

        //value of num1 assigned to temp
        temp = num1;

        //value of num2 assigned to num1
        num1 = num2;

        //value of temp (containing initial value of num1) assigned to num2
        num2 = temp;

        System.out.println("\nAfter swap:");
        System.out.println("Value 1 = " + num1);
        System.out.println("Value 2 = " + num2);

        sc.close(); //close scanner
    }
}
