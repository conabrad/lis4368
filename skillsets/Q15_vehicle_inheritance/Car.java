public class Car extends Vehicle
{
    //instance varibales (no static keyword): each object has own set of instance variables
    private float speed;

     //Java doesnt support parameters with default values (like C#. PHP and C++)
    //default constructor
    public Car()
    {   
        //super() must be first element in cosntructor that calls construtor in its superclass.
        //also only call super() in constructor.
        super();
        System.out.println("\nInside car default constructor.");
        speed = 100;
    }

    //parameterized constructor
    public Car(String m, String d, int y, float s)
    {
        super(m,d,y);
        System.out.println("\nInside vehcile constructor with parameters.");
        speed = s;
    }

    //getter/setter methods (accessor/mutator)
    public float getSpeed()
    {
        return speed;
    }

    public void setSpeed(float s)
    {
        //set instance vehicle value to parameter value
        speed = s;
    }

    public void print()
    {
        super.print();
        System.out.println(", Speed: " + speed);
    }
}