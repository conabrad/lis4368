public class Vehicle
{
    //instance varibales (no static keyword): each object has own set of instance variables
    private String make;
    private String model;
    private int year;
    
    //Java doesnt support parameters with default values (like C#. PHP and C++)
    //default constructor
    public Vehicle()
    {
        System.out.println("\nInside vehcile default constructor.");
        make = "My Make";
        model = "My Model";
        year = 2001;
    }

    //parameterized constructor
    public Vehicle(String make, String model, int year)
    {
        System.out.println("\nInside vehcile constructor with parameters.");
        this.make = make;
        this.model = model;
        this.year = year;
    }

    public String getMake()
    {
        return make;
    }

    public void setMake(String mk)
    {
        //set instance vehicle value to parameter value
        make = mk;
    }
    
    public String getModel()
    {
        return model;
    }

    public void setModel(String md)
    {
        model = md;
    }
    
    public int getYear()
    {
        return year;
    }

    public void setYear(int y)
    {
        year = y;
    }
    
    public void print()
    {
        System.out.print("Make: " + make + ", Model: " + model + ", Year: " + year);
    }
}