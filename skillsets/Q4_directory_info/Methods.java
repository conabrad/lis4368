import java.io.File;
import java.util.Scanner;

public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("Program lists files and subdirectories of *user*-specfified directory.\n");
    }
    
    public static void directoryInfo()
    {
        //initialize variable
        String userDir = "";
        Scanner sc = new Scanner(System.in);

        //Demo user-entered directory path (note: escaped backslash!):
        //C:\\tomcat\\webapps\\lis4368
        System.out.print("Please enter directory path: ");
        userDir = sc.nextLine(); //read user input

        //create file object for user-specified directory
        //File directoryPath = new File("C:\\tomcat\\...")
        File directoryPath = new File(userDir);

        File fileList[] = directoryPath.listFiles();

        System.out.println("List files and directories in specified directory:");
        for(File file : fileList)
        {
            System.out.println("Name: " + file.getName());
            System.out.println("Path: " + file.getAbsolutePath());
            System.out.println("Size (Bytes): " + file.length());
            System.out.println("Size (KB): " + file.length()/(1024));
            System.out.println("Size (MB): " + file.length()/(1024*1024));
            System.out.println();
        }

        //Note: File Size in File.length diff from Windows explorer
        //https://Stackoverflow.com/questions/28885037/file-size-in-file-length-different-from-windows-explorer

        /*
        try {
        
            File fileList[] = directoryPath.listFiles();

        System.out.println("List files and directories in specified directory:");
        for(int i = 0; i <files.Length; i++)
        {
            System.out.println("Name: " + file.getName());
            System.out.println("Path: " + file.getAbsolutePath());
            System.out.println("Size (Bytes): " + file.length());
            System.out.println("Size (KB): " + file.length()/(1024));
            System.out.println("Size (MB): " + file.length()/(1024*1024));
            System.out.println();
        }

    }

    catch (Exception e)
    {
        System.err.println(e.getMessage());
    }
        */
    sc.close();
    }
}
