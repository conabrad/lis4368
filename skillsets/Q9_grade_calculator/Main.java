//javadoc: documentation generator for generating
//API documentation in HTMl ofrmat from Java source code. 
//https://www.oracle.com/technical-resources/articles/java/javadoc-tool.html
class Main 
{
    public static void main(String arg[]) 
    {
        //call static void methods (i.e, no object, non-value returning)
        Methods.getRequirements();
        Methods.getScore();
    }   
}
