public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("Program loops through array of floats.");
        System.out.println("Use the following values: 1.0, 2.1, 3.2, 4.3, 5.4");
        System.out.println("Use follwoing loop structures: for, enchanced for, while, do...while.");
        System.out.println();
    }
    
    public static void arrayIteration()
    {
        float fltVals[] = {1.0F,2.1f,3.2f,4.3f,5.4f};

        System.out.println("for loop: ");
        for (int i = 0; i < fltVals.length; i++)
        {
            System.out.println(fltVals[i]);
        }

        System.out.println("\nenhanced for loop: ");
        for (float test : fltVals)
        {
            System.out.println(test);
        }
        
        System.out.println("\nwhile loop: ");
        int i=0;
        while (i < fltVals.length)
        {
            System.out.println(fltVals[i]);
            i++;
        }
        
        i=0;
        System.out.println("\ndo...while loop: ");
        do
        {
            System.out.println(fltVals[i]);
            i++;
        }
        while (i < fltVals.length);
    }
}
