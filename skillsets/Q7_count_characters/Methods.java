import java.util.Scanner;

public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("Program Requirements:");
        System.out.println("\t1. Counts numbers and types of characters from user-entered string.");
        System.out.println("\t2. Count: total, letters (upper-/lower-case), numbers, spaces, and other characters.\n\tHint: Helpful methods: isLetter(), isDigit(), isSpaceChar(), and others. :D");
    }
    
    public static void countChar()
    {
        Scanner sc = new Scanner(System.in);
        String str = "";
        int letter = 0;
        int space = 0;
        int num = 0;
        int upper = 0;
        int lower = 0;
        int other = 0;

        System.out.print("Please enter string: ");
        str = sc.nextLine();

        char[] ch = str.toCharArray();

        for(int l = 0; l < str.length(); l++)
            {
                if(Character.isLetter(ch[l]))
                {
                    //if letter test for upper and lower case
                    if(Character.isUpperCase(ch[l]))
                    {
                        upper++;
                    }
                    if(Character.isLowerCase(ch[l]))
                    {
                        lower++;
                    }
                    letter++;
                }
                else if(Character.isDigit(ch[l]))
                {
                    num++;
                }
                else if(Character.isSpaceChar(ch[l]))
                {
                    space++;
                }
                else
                {
                    other++;
                }
            }
        System.out.println("\nYour string: \"" + str + "\" has the following number and types of characters:");
        
        System.out.println("Total number of characters: " + str.length());
        System.out.println("Letter(s): " + letter);
        System.out.println("Upper-case letter(s): " + upper);
        System.out.println("Lower-case letter(s): " + lower);
        System.out.println("Number(s): " + num);
        System.out.println("Space(s): " + space);
        System.out.println("Other character(s): " + other);

    sc.close(); //close scanner
        
    }
}
