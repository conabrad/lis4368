import java.util.Scanner;

public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("Program lists system properties.");
    }
    
    public static void lineAnalysis()
    {
        Scanner sc = new Scanner(System.in);

        String userLine = "";
        char userChar = ' ';
        int charCount = 0;
        

        System.out.print("Enter line of text: ");
        while(!sc.hasNextLine())
        {
            System.out.println("Invalid input!\n");
            sc.nextLine();//important! if omitted, will go into infinite loop on invalid input!
            System.out.print("Please try again. Enter your first value: ");
        }
        userLine = sc.nextLine();//valid input

        System.out.print("Enter character to check: ");
        while(!sc.hasNextLine())
        {
            System.out.println("Invalid input!\n");
            sc.nextLine();//important! if omitted, will go into infinite loop on invalid input!
            System.out.print("Please try again. Enter your first value: ");
        }
        userChar = sc.next().charAt(0);//valid input

        for(int i = 0; i < userLine.length(); i++)
        {
            if(userLine.charAt(i) == userChar)
            {
            charCount++;
            }
        }

        System.out.println("Number of characters in line of text: " + userLine.length());
        System.out.println("The character " + userChar + " appears " + charCount + " time(s) in the line of text." );
        System.out.println("ASCII value: " + (int) userChar); //cast char to int
    }
}
