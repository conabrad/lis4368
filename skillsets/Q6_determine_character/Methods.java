import java.util.Scanner;


public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("Program determines whether use-entered value is vowel, consonant, special character, or an integer." +
        "\nProgram displays character's ASCII value." + 
        "\n\nRef:\nASCII Background: https://en.wikipedia.org/wiki/ASCII\nASCII Character Table: https://www.ascii-code.com/\nLookup Tables: https://www.lookuptables.com/");
    }
    
    public static void determineChar()
    {
        //init vars
        char ch = ' ';//need space for char
        char chTest = ' ';
            Scanner sc = new Scanner(System.in);

        System.out.print("Please enter a character: ");
        //next() func returns next token
        //Token: smallest elements of a prgm meaningful to compli/interpe
        //generally, inditifiers, keywords, literals, operators, and punctuations
        //Note: whtie space and comments not tokens--though, separate tokens. 
        //ex: "I like this" I is 1st token like is second this is third
        ch = sc.next().charAt(0);//first token
        chTest = Character.toLowerCase(ch);

        //Note: can be resolved in diff ways
        //Here works w legacy java, no special funcs
        //test for alpha chars
        if(chTest == 'a' || chTest == 'e' || chTest == 'i' || chTest == 'o' || chTest == 'u' || chTest == 'y')
        {
            System.out.println(ch + " is a vowel. ACII value: " + (int) ch); //cast char to int
        }
        //test for nums
        else if(ch >= '0' && ch <= '9')
        {
            System.out.println(ch + " is an int. ACII value: " + (int) ch); //cast char to int
        }
        //test for consonants(vowels above)
        else if((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
        {
            System.out.println(ch + " is a consonant. ACII value: " + (int) ch); //cast char to int
        }
        //otherwise its special
        else
        {
            System.out.println(ch + " is a special character. ACII value: " + (int) ch); //cast char to int
        }
    sc.close();
    }
}
