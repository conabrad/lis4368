import java.util.Scanner;
import java.io.*; //provides input output streams used to read/write data to input/output sources

public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("Program Requirements:");
        System.out.println("Program captures user input, writes to and reads from same file, and counts number of words in file." + 
        "\nHint: use hasNext() method to read number of words (tokens)\n");
    }
    
    public static void getWriteRead()
    {
        String myFile = "filecountwords.txt";

        try
        {
            //create File object
            File file = new File(myFile); //takes file name as argument

            //create PrintWriter object
            PrintWriter writer = new PrintWriter(file); //takes file as argument

            //create Scanner object for user input
            Scanner input = new Scanner(System.in);

            //create String object object to store user input
            String str = "";

            System.out.print("Please enter line of text: ");
            str = input.nextLine();

            //write to file using PrintWriter write() method
            writer.write(str);

            System.out.println("Saved to file \"" + myFile + "\"");

            //close input Scanner object
            input.close();
            //close input file--otherwise, open PrintWriter stream
            writer.close();

            //Scanner read = new Scanner(new FileInputStream(file));
            Scanner read = new Scanner(new FileInputStream(file));
            int count = 0;
            while(read.hasNext())
            {
                read.next();
                count++;
            }
            System.out.println("Number of words: " + count);

            read.close();
        }
        catch(IOException ex)
        {
            System.out.println("Error writing to or reading from file '" + myFile + "'");
        }
    }
}
