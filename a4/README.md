| Developed by: Conner A. Bradley |
| ------------------------------: |
# Web Application Development Assignment 4 

***
***

### Requirements:

1. Code data entry forms with basic server-side validation using regular expressions and pass/fail tests into your web application. 
1. Develop separate programs to do the following:
    * SS10 - Perform addition, subtraction, multiplication, division, and powers floating point numbers. Rounds to two decimal places. Checks for non-numeric values and division by zero.
    * SS11 - Calculate amount of money saved for a period of years, at a monthly compound interest rate. Returned amount is formatted in U.S. currency, and rounded to two decimal places. Performs data validation for non-numeric values, as well as only integer values for years. 
    * SS12 - Create a string array (str1) based upon users' number of preferred programming languages. Create a second string array (str2) based upon the length of str1 array. Copy str1 array elements into str2. Print elements of both arrays using Java's enhanced for loop. 
1. Develop README.md in markdownload language and push to repository when finished. 
1. Display screencaptures of your results.
1. Review the chapter questions for chapters 11 - 12. 


### Results:
>**NOTE:** Click any screencapture to view an enlarged version

| *Failing Data*: | *Passing Data*: |
|:---------------:|:---------------:|
| [![Data Validation Fail Screenshot](img/form_fail.png "Data validation failure")](img/form_fail.png) | [![Data Validation Pass Screenshot](img/form_pass.png "Data validation pass")](img/form_pass.png) |

| *Skillset 10 - Simple Calculator*: | *Skillset 11 - Compound Interest Calculator*: | *Skillset 12 - Array Copy*: |
|:-------------------------------------------:|:-------------------------------------------:|:------------------------------------------------:|
[![Q10_simple_calculator Screenshot](img/Q10_simple_calculator.png)](img/Q10_simple_calculator.png) | [![Q11_compound_interest_calculator Screenshot](img/Q11_compound_interest_calculator.png)](img/Q11_compound_interest_calculator.png) | [![Q12_array_copy Screenshot](img/Q12_array_copy.png)](img/Q12_array_copy.png)

### Referenced in README.md:

* project requirements
* form_fail.png
* form_pass.png
* skillsets:
    * Q10_simple_calculator.png
    * Q11_compound_interest_calculator.png
    * Q12_array_copy.png