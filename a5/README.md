| Developed by: Conner A. Bradley |
| ------------------------------: |
# Web Application Development Assignment 5 

***
***

### Requirements:

1. Code data entry forms with basic server-side validation using regular expressions and pass/fail tests into your web application. 
1. Develop separate programs to do the following:
    * SS13 - Read and write into a file.
    * SS14 - Use Object Oriented Programming to get and set vehicle year, make, and model in constructor objects. 
    * SS15 - Use inheritance to get and set vehicle speed into the previous skillset's vehcile construct. 
1. Develop README.md in markdownload language and push to repository when finished. 
1. Display screencaptures of your results.
1. Review the chapter questions for chapters 13 - 15. 


### Results:
>**NOTE:** Click any screencapture to view an enlarged version

| *Failing Data*: | *Passing Data*: | *SQL Customer Table*: |
|:---------------:|:---------------:| :---------------:|
| [![Data Validation Fail Screenshot](img/thanks.png "Data validation failure")](img/thanks.png) | [![Data Validation Pass Screenshot](img/valid.png "Data validation pass")](img/valid.png) | [![SQL Data Entry](img/sql.png "SQL Tables")](img/sql.png) |

| *Skillset 13 - Read/Write a File*: | *Skillset 14 - Vehicle Constructor Class*: | *Skillset 15 - Constructors With Inheritance*: |
|:-------------------------------------------:|:-------------------------------------------:|:------------------------------------------------:|
[![Q13_file_write_read_count_words Screenshot](img/Q13_file_write_read_count_words.png)](img/Q13_file_write_read_count_words.png) | [![Q14_vehicle Screenshot](img/Q14_vehicle.png)](img/Q14_vehicle.png) | [![Q15_vehicle_inheritance Screenshot](img/Q15_vehicle_inheritance.png)](img/Q15_vehicle_inheritance.png)

### Referenced in README.md:

* project requirements
* thanks.png
* valid.png
* sql.png
* skillsets:
    * Q13_file_write_read_count_words.png
    * Q14_vehicle.png
    * Q15_vehicle_inheritance.png