
| Developed by: Conner A. Bradley |
| ------------------------------: |
# Web Application Development Project 1 

***
***

### Requirements:

1. Design your web application's splash page.
1. Code data entry forms with basic client-side validation using regular expressions and pass/fail tests into your web application. 
1. Develop separate programs to do the following:
    * SS7 - Count numbers and types of characters from a user-entered string.
    * SS8 - Lists all ASCII values for characters and outputs a character based on a user-entered ASCII value.
    * SS9 - Returns a letter grade based upon a user-entered numeric score.
1. Develop README.md in markdownload language and push to repository when finished. 
1. Display screencaptures of your results.
1. Review the chapter questions for chapters 9 - 10. 


### Results:
>**NOTE:** Click any screencapture to view an enlarged version

| *Splash Screen*: | *Failing Data*: | *Passing Data*: |
|:-----------------------:|:------------------------------:|:-------------------------:|
| [![Online Portfolio Splash Screenshot](img/splash.png "Splash screen")](img/splash.png) | [![Data Validation Fail Screenshot](img/data_valid_fail.png "Data validation failure")](img/data_valid_fail.png) | [![Data Validation Pass Screenshot](img/data_valid_pass.png "Data validation pass")](img/data_valid_pass.png) |

| *Skillset 7: Counting Characters*: | *Skillset 8: ASCII*: | *Skillset 9: Grade Calculator*: |
|:-------------------------------------------:|:-------------------------------------------:|:------------------------------------------------:|
[![Q7_counting_chars Screenshot](img/Q7_counting_chars.png)](img/Q7_counting_chars.png) | [![Q7_counting_chars Screenshot](img/Q8_ascii.png)](img/Q8_ascii.png) | [![Q9_grade_calculator Screenshot](img/Q9_grade_calculator.png)](img/Q9_grade_calculator.png)

### Referenced in README.md:

* project requirements
* splash.png
* data_valid_fail.png
* data_valid_pass.png
* skillsets:
    * Q7_counting_chars.png
    * Q8_ascii.png
    * Q9_grade_calculator.png